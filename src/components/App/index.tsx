import React from 'react';
import './index.css';
import { HelloWorld } from "../index";

const App = () => {
  return (
    <div className="App">
      <HelloWorld />
    </div>
  );
}

export default App;
